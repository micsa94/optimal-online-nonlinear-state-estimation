function [X_pos, MSE] = Improved_Extended_Kalman_Filter(Q, R, V, N, X0, X0_mean, Samples, Estimations, X, Z, P_pos)
%% 
X_pos(1) = X0_mean;%Setting x(0) positive as mean x0 value to be used for estimated values

P_pos(1) = P_pos;%Estimated error covariance is the mean value of p 


%%
for i=2:Samples 
    
    %Stage 1
    L0 = sqrt(P_pos(i-1));
    X_prev(1) = X_pos(i-1);
    X_prev(2) = X_pos(i-1) + sqrt(2)*L0;
    X_prev(3) = X_pos(i-1) - sqrt(2)*L0;
    
    for j=1:3
        X_curr(j) = (X_prev(j)/2)+(((25*X_prev(j))/(1+X_prev(j)^2)))+(8*cos(1.2*(i-1)));
    end
    
    A(1)=1/2;
    A(2)=1/4;
    A(3)=1/4;
    X_neg = dot(A,X_curr');
    
    for j=1:3
        X_prod(j) = ((X_curr(j)-X_neg)*(X_curr(j)-X_neg)');
    end
    
    P_neg = dot(A,X_prod)+Q;
    
    %Stage 2
    Lk=sqrt(P_neg);
    X_curr(1) = X_neg;
    X_curr(2) = X_neg + sqrt(2)*Lk;
    X_curr(3) = X_neg - sqrt(2)*Lk;
    X_neg = dot(A,X_curr');
    
    for j=1:3
        Y_curr(j) = (X_curr(j)^2/20);
    end
    Y_est = dot(A,Y_curr');
    
    for j=1:3
        P_prod_y(j) = ((Y_curr(j)-Y_est)*((Y_curr(j)-Y_est)'));
    end
    P_y = dot(A,P_prod_y) + R;
    
    for j=1:3
        P_prod_xy(j) = ((X_curr(j)-X_neg)*((Y_curr(j)-Y_est)'));
    end
    P_xy = dot(A,P_prod_xy);
    K = (P_xy)*(P_y^-1);
    X_pos(i) = X_neg + K*(Z(i)-Y_est);
    P_pos(i) = P_neg - K*P_y*K';
end

%%
MSE = 1/Samples* sum((X - X_pos).^2);

T = 0:Samples-1;
figure(6);
clf
plot(T,X,'.-b',T,X_pos,'.-r');
str = sprintf('IEKF MSE = %f',MSE);
title(str);