function [X_out, MSE] = Particle_Filter(V,N,Q,R,Samples,X0_mean)
%%
Estimations = 100;

%%
X=zeros(Estimations, Samples);
X_out = zeros(1,Samples);
X_out(1) = X0_mean;
X_real=X_out(1);
W=zeros(Estimations,1);

%%
for i=1:Estimations
    W(i)=1/Estimations;
end

%%
for i=2:Samples
    X_real(i) = (X_real(i-1)/2)+(((25*X_real(i-1))/(1+X_real(i-1)^2)))+(8*cos(1.2*(i-1)))+V(i-1);
    Z(i) = ((X_real(i)^2)/20) + N(i);    
end

%%
X(:,1)=normrnd(X_out(1),1,Estimations,1);
for samp=2:Samples
    for i=1:Estimations
        X(i,samp)= X(i,samp-1)/2 + (25*X(i,samp-1))/(1+X(i,samp-1)^2) + V(samp-1);
        for j=1:Estimations
            f_x = X(j,samp-1)/2 + (25*X(j,samp-1))/(1+X(j,samp-1)^2);
            W_k(j)= (W(j)*(1/(sqrt(2*pi)*Q))*exp(-(1/(Q*2))*(X(i,samp)- f_x)^2));
        end
        W_k(i)=sum(W_k);
    end
    for i=1:Estimations
        h_x=X(i,samp)^2/20;
        for j=1:Estimations
            h__x=X(i,samp)^2/20;
            W(j)=(W_k(j)*(1/(sqrt(2*pi)*R))*exp(-(1/(R*2))*(Z(samp)- h__x)^2));
        end
        summ=(sum(W));
        if summ==0
            summ=1;
        end
        W(i)=(W_k(i)*(1/(sqrt(2*pi)*R))*exp(-(1/(R*2))*(Z(samp)- h_x)^2))/summ;
        
    end
    X_out(samp)=sum(W'*X(:,samp));
end

%%
MSE =1/Samples* sum((X_real - X_out).^2)
N=0:Samples-1;
figure(7);
clf
plot(N,X_real,'.-b',N,X_out,'.-r');
str = sprintf('GBF MSE = %f',MSE);
title(str);