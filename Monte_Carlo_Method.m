function [] = Monte_Carlo_Method()
%% Variables
Estimations = 2000;
Samples = 101;
Q = 10;
R = 1;
X0 = normrnd(0, 1, Estimations,1);
X=zeros(Estimations,Samples);
X(:,1)=X0;
V=normrnd(0,Q,Estimations,Samples);
N=normrnd(0,R,Estimations,Samples);

%% Monte Carlo Method Estimations
for j=1:Estimations
    for i=2:Samples
        X(j,i) = ((X(j,i-1)/2)+(25*X(j,i-1))/(1+X(j,i-1)^2)+8*cos(1.2*(i-1))+V(j,i-1));
        Z(j,i) = X(j,i)^2/20 + N(j,i); 
    end
end

%% Plot PDFs
figure(1)
Bins=50;
histogram(X(:,2),Bins)
title('pdf p(x1)')
figure(2)
histogram(X(:,51),Bins)
title('pdf p(x50)')
figure(3)
histogram(X(:,101),Bins)
title('pdf p(x100)')'

