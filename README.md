# Optimal Online Nonlinear State Estimation

The aim of this project is to use different methods to estimate states of a given nonlinear system which can be found in the report.

The program consists of 9 different script files.

To run the program the following steps need to be taken.

1. Open Menu script file.

2. Press the Monte Carlo Method to view pdfs p(x1), p(x50) and p(x100).
   (The code for this method can be view in the Monte_Carlo_Method script file)

3. Press Generate one Experiment to gather the initial variables.
   (The code for this can be seen in the Generate_Measurements script file)

4. Due to the grid based method response giving NaN for some experiments, 
   press the Grid Based Filter first. If NaN is given press Generate one Experiment again.
   (The code for this can be seen in the Grid_Based_Method script file)

5. Press the Extended Kalman Filter button
   (The code for this can be seen in Extended_Kalman_Filter script file)

6. Press the Particle Filter button
   (The code for this can be seen in Particle_Filter script file)

7. Press the Improved Extended Kalman Filter button
   (The code for this can be seen in Improved_Extended_Kalman_Filter script file)

8. Press the Improved Grid Based Filter button
   (The code for this can be seen in Improved_Grid_Based_Filter script file)

9. Press the Improved Particle Filter button
   (The code for this can be seen in Improved_Particle_Filter script file)

10. Press the Comparison of Methods button to view all the results at once.

11. Press the Exit Program Button to exit.