function [Output_est_x, MSE] = Particle_Filter(Vi,Ni,Samples,X,Z,No_of_init_part,P)
%%
for i=1:Samples
    for j=1:No_of_init_part
        P_update(j) = ((P(j)/2)+(25*P(j))/(1+P(j)^2)+Vi(i,j));
        Z_update(j) = P_update(j)^2/20+Ni(i,j); 
        Qi(j)=(1/sqrt(2*pi))*exp(-(Z(i)-Z_update(j))^2/(2));
    end
    Qi=Qi/sum(Qi);
    for j=1:No_of_init_part
        A = P_update(find(rand <= cumsum(Qi),1));
        if isempty(A)
            A=0;
        end
        P(j)=A;
    end
    Est_x=mean(P);
    Output_est_x(i)=Est_x;
end

%%
MSE =1/Samples* sum((X - Output_est_x).^2)
N=0:Samples-1;
figure(9);
clf
plot(N,X,'.-b',N,Output_est_x,'.-r');
str = sprintf('PF MSE = %f',MSE);
title(str);

