function [X_pos, MSE] = Extended_Kalman_Filter(Q, R, V, N, X0, X0_mean, Samples, Estimations, X, Z, P_pos)
%% 
X_pos(1) = X0_mean;%Setting x(0) positive as mean x0 value to be used for estimated values

P_pos(1) = P_pos;%Estimated error covariance is the mean value of p 

I=eye(1);%Identity matrix 

%%
for i=2:Samples 
    
    %Stage 1
    X_neg = (X_pos(i-1)/2)+(((25*X_pos(i-1))/(1+X_pos(i-1)^2)))+(8*cos(1.2*(i-1)));
    F_prev = (1/2)+(((25*(1+X_pos(i-1)^2))-(25*(X_pos(i-1))*(2*X_pos(i-1))))/((1+X_pos(i-1)^2)^2));
    L = 1;
    P_neg = (F_prev*P_pos(i-1)*F_prev')+(L*Q*L');
    
    %Stage 2
    H = X_neg/10;
    M = 1;
    K_k = P_neg*H'*(H*P_neg*H' + M*R*M')^-1;
    P_pos(i) = (I-K_k*H)*P_neg;
    X_pos(i) = X_neg + K_k*(Z(i)-((X_neg^2)/20));
    
end

%%
MSE = 1/Samples* sum((X - X_pos).^2);

T = 0:Samples-1;
figure(5);
clf
plot(T,X,'.-b',T,X_pos,'.-r');
str = sprintf('EKF MSE = %f',MSE);
title(str);

