%%
clc;
clear all;
close all;

%%
Option = 1; % Initialize
while Option < 10
  Option = menu('Optimal State Estimation','','Generate one Experiment','Extended Kalman Filter','Grid Based Filter',...
             'Particle Filter','Improved Extended Kalman Filter','Improved Grid Based Filter','Improved Particle Filter','Comparison of Methods','Exit Program')
  if Option == 1
    Monte_Carlo_Method();
  end
  if Option == 2
    [Q,R,V,Vi,N,Ni,X0,X0_mean,Samples,Estimations,X,Z,P_pos,No_of_init_part, Part] = Generate_Measurements();
  end
  if Option == 3
    [EKF_X, EKF_MSE] = Extended_Kalman_Filter(Q, R, V, N, X0, X0_mean, Samples, Estimations, X, Z, P_pos);
  end
  if Option == 4
    [GBF_X, GBF_MSE] = Grid_Based_Method(V,N,Q,R,Samples,X0_mean);
  end
  if Option == 5
    [PF_X, PF_MSE] = Particle_Filter(Vi,Ni,Samples,X,Z,No_of_init_part,Part);
  end
  if Option == 6
    [IEKF_X, IEKF_MSE] = Improved_Extended_Kalman_Filter(Q, R, V, N, X0, X0_mean, Samples, Estimations, X, Z, P_pos);
  end
  if Option == 7
    [IGBF_X, IGBF_MSE] = Improved_Grid_Based_Method(V,N,Q,R,Samples,X0_mean);
  end
  if Option == 8
    [IPF_X, IPF_MSE] = Improved_Particle_Filter(Vi,Ni,Samples,X,Z,No_of_init_part,Part);
  end
  if Option == 9
    T=0:Samples-1;
    figure(4);
    clf
    plot(T,X,'.-b',T,EKF_X,'.-r',T,IEKF_X,'.-g',T,PF_X,'.-c',T,IPF_X,'.-k',T,GBF_X,'.-y',T,IGBF_X,'.-m');
    str = sprintf('EKF MSE = %f, IEKF MSE = %f, PF MSE = %f, IPF MSE = %f, GBF MSE = %f, IGBF MSE = %f',EKF_MSE,IEKF_MSE, PF_MSE,IPF_MSE, GBF_MSE, IGBF_MSE);
    title(str);
    legend('Original','EKF','IEKF','PF','IPF','GBF', 'IGBF');
    
    Method = {'EKF';'IEKF';'GBF';'IGBF';'PF';'IPF'};
    MSE = {EKF_MSE;IEKF_MSE;GBF_MSE;IGBF_MSE;PF_MSE;IPF_MSE};
    T = table(Method,MSE)
  end
  if Option == 10
    close all;
    break;
  end
end