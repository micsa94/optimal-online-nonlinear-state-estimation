function [Output_est_x, MSE] = Improved_Particle_Filter(Vi,Ni,Samples,X,Z,No_of_init_part,P)
%% Particle Filter using Extended Kalman Filter 
Q=10;
R=1;
I=eye(1);
for i=1:No_of_init_part
    P_pos(i)=(P(i)-mean(P))*(P(i)-mean(P))';
end
P_pos = mean(P_pos);%Estimated error covariance is the mean value of p 

for i=2:Samples
    for j=1:No_of_init_part
        P_update = ((P(j)/2)+(25*P(j))/(1+P(j)^2)+Vi(i-1,j));
        F_prev = (1/2)+(((25*(1+P(j)^2))-(25*(P(j))*(2*P(j))))/((1+P(j)^2)^2));
        P_neg = F_prev*P_pos*F_prev + Q;
        H = P_update/10;
        K_k = P_neg*H'*(H*P_neg*H' + R)^-1;
        P(j) = P_update + K_k*(Z(i)- (P_update^2/20));
        P_pos=(I-K_k*H)*P_neg;
    end
    Est_x=mean(P);
    Output_est_x(i)=Est_x;
end

%%
MSE =1/Samples * sum((X - Output_est_x).^2)
N=0:Samples-1;
figure(10);
clf
plot(N,X,'.-b',N,Output_est_x,'.-r');
str = sprintf('IPF MSE = %f',MSE);
title(str);

