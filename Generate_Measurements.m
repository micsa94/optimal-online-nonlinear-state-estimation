function [Q,R,V,Vi,N,Ni,X0,X0_mean,Samples,Estimations,X,Z,P_pos,No_of_init_part, Part] = Generate_Measurements()
%%
Estimations=1000;%No of estimations for x0
Samples=101;%No of samples for x(no) from 0 <= k <= 100
No_of_init_part=202;
%% 
Q = 10;%Variance of noise v
R = 1;%Variance of noise n
V=normrnd(0,Q,1,Samples);%Gaussian noise v
N=normrnd(0,R,1,Samples);%Gaussian noise n
X0 = (normrnd(0, 1, Estimations,1));%Estimations for x0
X0_mean = mean(X0);%Mean value for x0
X = zeros(1,Samples);%Real x values 
X(1) = X0_mean;%Setting original value of x as mean value to be used for real values
%Expectation of (x0 - x0_mean) sqrt
for i=1:Estimations
    P(i)=(X0(i)-X0_mean)*(X0(i)-X0_mean)';
end
P_pos = mean(P);%Estimated error covariance is the mean value of p 

for i=1:Samples
    for j=1:No_of_init_part
        Vi(i,j)=V(i)+sqrt(2)*randn;
        Ni(i,j)=N(i)+sqrt(2)*randn;
    end
end

No_of_init_part=202;
%%
%Random particles based on prior gaussian distribution
for i=1:No_of_init_part
    Part(i)=X0_mean+sqrt(2)*randn;
end
%%
for i=2:Samples
    X(i) = (X(i-1)/2)+(((25*X(i-1))/(1+X(i-1)^2)))+(8*cos(1.2*(i-1)))+V(i-1);
    Z(i) = ((X(i)^2)/20) + N(i);    
end

